#!/bin/bash
#On utilise le chemin en paramètre, à défaut, on prend la racine
if [[ $# -eq 1 ]]; then
        /opt/hadoop/bin/hadoop fs -ls -R $1 | awk '{print $8}' | \sed -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'
else
        /opt/hadoop/bin/hadoop fs -ls -R / | awk '{print $8}' | \sed -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'
fi
