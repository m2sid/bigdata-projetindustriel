#!/bin/bash
FILE=$1
DEST="$FILE.password"

#Suppression des utilisateurs du groupe et de leurs dossier sur HDFS
for student in $(cat $1); do
	/opt/hadoop/bin/hadoop fs -rm -r /user/$student
	echo "Home HDFS of $student removed"
	deluser $student --remove-home
	echo "User:$student removed"
done

#Suppression du groupe utilisateur
echo "Group:$FILE removed"
groupdel $FILE

#Suppression du fichier d'utilisateurs
rm $FILE
echo "File:$FILE removed"

#Suppression du fichier de login:password s'il existe
if [[ -e $DEST ]]; then
	rm $DEST
	echo "File:$DEST removed"
fi
