#!/bin/bash
FILE=$1

#Création du fichier de login/password
DEST="$FILE.password"
touch $DEST
chown operrin:operrin $DEST
chmod 700 $DEST

#Création du groupe d'utilisateurs
groupadd $FILE

#Création de chaque utilisateur avec un password et un dossier HDFS générés
for student in $(cat $1); do
	adduser $student --gecos ",,," --disabled-password
	usermod -a -G $FILE $student
	pwd=$(openssl rand -base64 5)
	echo "$student:$pwd" | chpasswd
	echo "$student:$pwd">> $DEST
	/opt/hadoop/bin/hadoop fs -mkdir /user/$student
	/opt/hadoop/bin/hadoop fs -chown -R $student:hdadmin /user/$student
	/opt/hadoop/bin/hadoop fs -chmod -R 770 /user/$student
done
