#!/bin/bash
echo "Start Format"
/opt/hadoop/bin/hadoop fs -rm -r /user
/opt/hadoop/bin/hadoop fs -rm -r /data
/opt/hadoop/bin/hadoop fs -rm -r /tmp

/opt/hadoop/bin/hadoop fs -mkdir /user
/opt/hadoop/bin/hadoop fs -chown hadoop:hdadmin /user
/opt/hadoop/bin/hadoop fs -chmod 775 /user

/opt/hadoop/bin/hadoop fs -mkdir /data
/opt/hadoop/bin/hadoop fs -chown hadoop:hdadmin /data
/opt/hadoop/bin/hadoop fs -chmod -R 775 /data

/opt/hadoop/bin/hadoop fs -mkdir /tmp
/opt/hadoop/bin/hadoop fs -chown hadoop:hdadmin /tmp
/home/hadoop/scripts/stop-hadoop.sh
/home/hadoop/scripts/start-hadoop.sh
/opt/hadoop/bin/hadoop fs -chmod -R 777 /tmp

users=$(awk -F: '/^hdadmin/ {print $4;}' /etc/group | sed 's/,,*/ /g')
for user in $users;
do
	/opt/hadoop/bin/hadoop fs -mkdir /user/$user
	/opt/hadoop/bin/hadoop fs -chown -R $user:hdadmin /user/$user
	/opt/hadoop/bin/hadoop fs -chmod -R 770 /user/$user
done
echo "Format HDFS has been successfully completed"
